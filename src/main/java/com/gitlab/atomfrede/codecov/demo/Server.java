package com.gitlab.atomfrede.codecov.demo;

import static spark.Spark.awaitInitialization;
import static spark.Spark.stop;

public class Server {

    public static void main(String[] args) {
        // Start spark without waiting for ignition
        igniteSpark(false);
    }

    public static void igniteSpark(boolean wait) {


        if (wait) {
            awaitInitialization();
        }

    }

    public static void extinguishSpark() {
        stop();
    }

    public static boolean returnTrue() {

        return true;
    }
}
